all: prog

prog: main.o element.o table.o
	g++ bin/main.o bin/element.o bin/table.o -o bin/prog

main.o: src/main.cpp src/table.h
	g++ -Wall -c src/main.cpp -o bin/main.o

element.o: src/element.cpp src/element.h
	g++ -Wall -c src/element.cpp -o bin/element.o

table.o: src/table.cpp src/table.h src/element.h
	g++ -Wall -c src/table.cpp -o bin/table.o

clean: 
	rm bin/*.o

veryclean: clean
	rm bin/prog

run: 
	bin/prog