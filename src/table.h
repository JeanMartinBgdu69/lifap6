#ifndef	TABLE_H
#define TABLE_H

#include "element.h"


typedef struct
{
  int taille;
  element * t; // table de hachage
  int (*hfunct)(element, int); //pointeur de fonction de hachage
  int (*rhfunct)(int, int, int); //       "       "        " de rehachage
  int * p; // table de meme taille que table de hachage pour definir les cases occupées
}Htable;

void initH(Htable* t, int taille); //fonction d'initialisation
bool tablePleine(Htable t);
int hmodulo(element e, int taille);
int hextract(element e, int taille);/// hachage par extraction de 1 bit sur 2;
int hmulti(element e, int taille);
void insertionTable(Htable* t, element e);
int rhlin(int indice, int compteur, int taille);
int rhquadra(int indice, int compteur, int taille);
int rechercheTable(Htable* t, element e);
void suppresionTable(Htable * t, element e);
void afficheOccupe(Htable t);
void afficheTable(Htable t);

    #endif
