#include "table.h"
#include <iostream>
#include <cstdlib>
#include <math.h>

void initH(Htable* t, int taille) //fonction d'initialisation
{
	t->t = new element[taille]; // allocation table de hachage de taille taille
	t->p = new int[taille]; // allocation table occupation de taille taille
	for(int i=0;i < taille; i++){
	  t->p[i] = -1;
	}
	t->taille = taille;
}
bool tablePleine(Htable t)
{
	for (int i = 0; i < t.taille; ++i)
	{
		if(t.p[i] == 0)
		{
			return false;
		}
	}
	return true;
}
int hmodulo(element e, int taille)
{
	return e.cle%taille;

}

int hextract(element e, int taille){
  return e.cle % taille;

}

int hmulti(element e, int taille){
  double q = (sqrt(5) +1 )/2; ///q nb or
  return (fmod(multiplicationCle(e, q),  1.0) * taille);///fmod car % pas sur double et cast en int
}

int rhlin(int indice, int compteur, int taille){
  return (indice+1)%taille;
}

int rhquadra(int indice, int compteur, int taille)
{
	return (indice+compteur)%taille;
}


void insertionTable(Htable* t, element e)
{
	int j=1;
	int i;
	i = t->hfunct(e, t->taille);
	if(tablePleine(*t))
	{
		while(t->p[i] == 1)
		{
			i = t->rhfunct(i, j, t->taille);
			j++;
		}
		t->t[i] = e;
		t->p[i] = 1;
	}
	else
	{
		std::cout << "La Table est pleine" << std::endl;
	}
}


 int rechercheTable(Htable* t, element e){
  int i = t->hfunct(e, t->taille);
  int j = 0;
  if(t->p[i] == -1) return -1;///cas simple, element pas dans la table au  premier hachage
  if((t->t[i].cle == e.cle) && (t->p[i] != 0)) return i;
  i = t->rhfunct(i, j, t->taille);
  j++;
  while((t->p[i]!=-1) && (i != t->hfunct(e, t->taille))){ ///tant que t est remplie et i pas revenu au depart
    if((t->t[i].cle == e.cle)&& (t->p[i] != 0)) return i;
    i = t->rhfunct(i, j, t->taille);
    j++;
  }
  return -1;
}

void suppresionTable(Htable * t, element e){
  int i = rechercheTable(t, e);
  if (i != -1){
    t->p[i] = 0;
  }
  
}


void afficheTable(Htable t)
{
	for (int i = 0; i < t.taille; ++i)
	{
		afficheElement(t.t[i]);
		std::cout << " | ";
	}
	std::cout << std::endl;
}

void afficheOccupe(Htable t)
{
	for (int i = 0; i < t.taille; ++i)
	{
		std::cout << t.p[i] << " | ";
	}
	std::cout << std::endl;
}
