#ifndef	ELEMENT_H
#define ELEMENT_H

typedef unsigned int key;

typedef struct
{
	key cle;
	int age;
}element;

void afficheElement(element e);
double multiplicationCle(element e, double x);
element nouveauElement();

#endif
