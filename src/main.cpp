#include "table.h"
#include <iostream>
#include <cstdlib>
#include <time.h>

using namespace std;

int main()
{
	int u;
	srand(time(NULL));
	unsigned int q;
	unsigned int p;
	Htable t;
	while((q <= 0) || (q > 3) || (p <= 0) || (p > 2))
	{
		system("clear");
		cout << "Choisissez une fonction de hachage :" << endl << endl;
		cout << "1 - modulo" << endl;
		cout << "2 - multiplier" << endl;
		cout << "3 - quitter" << endl;
		cout << endl;
		
		cin >> q;

		
		switch(q)
		{
			case 1:
			  t.hfunct = hmodulo;
				break;
			case 2:
			  t.hfunct = hmulti;
				break;
			default:
				break;
		}

		cout << "Choississez un rehachage" << endl << endl;
		cout << "1 - lineaire" << endl;
		cout << "2 - quadratique" << endl;
		cin >> p;

		switch(p){
		case 1:
		  t.rhfunct = rhlin;
		  break;
		case 2:
		  t.rhfunct = rhquadra;
		  break;
		default:
		  break;
		}
	}

	element e1, e2, e3, e4;
	e1 = nouveauElement();
	e2 = nouveauElement();
	e3 = nouveauElement();
	e4 = nouveauElement();

	afficheElement(e1);
	afficheElement(e2);
	afficheElement(e3);
	afficheElement(e4);

	int taille;
	std::cout << "Choississez la taille de la table de hachage :";
	std::cin >> taille;

	initH(&t, taille);
	afficheTable(t);
	afficheOccupe(t);
	insertionTable(&t, e1);
	afficheTable(t);
	afficheOccupe(t);
	insertionTable(&t, e2);
	afficheTable(t);
	afficheOccupe(t);
	insertionTable(&t, e3);
	afficheTable(t);
	afficheOccupe(t);
	insertionTable(&t, e4);
	afficheTable(t);
	afficheOccupe(t);
	return 0;
}
